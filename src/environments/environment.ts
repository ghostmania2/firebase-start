// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDP3WTyI0cuPACWTSFZNcofshi6nQxu1zw",
    authDomain: "fir-start-48200.firebaseapp.com",
    databaseURL: "https://fir-start-48200.firebaseio.com",
    projectId: "fir-start-48200",
    storageBucket: "fir-start-48200.appspot.com",
    messagingSenderId: "392423350233"
  }
};

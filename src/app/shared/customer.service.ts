import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { map } from 'rxjs/operators';

@Injectable()
export class CustomerService {

  constructor(private firebase: AngularFireDatabase) {}

  customerList: AngularFireList<any>;

  form = new FormGroup({
    $key: new FormControl(null),
    fullName: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required,Validators.email])
  });

  getCustomers() {
    this.customerList = this.firebase.list('customers');
    return this.customerList.snapshotChanges();
  }

  insertCustomer(customer) {
    this.customerList.push({
      fullName: customer.fullName,
      email: customer.email
    })
  }

  updateCustomer(customer) {
    this.customerList.update(customer.$key,{
      fullName: customer.fullName,
      email: customer.email
    })
  }

  removeCustomer($key: string) {
    this.customerList.remove($key);
  }

  editCustomer(customer) {
    this.form.setValue(customer);
  }

}
